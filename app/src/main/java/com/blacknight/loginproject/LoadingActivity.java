package com.blacknight.loginproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import static android.view.View.VISIBLE;

public class LoadingActivity extends AppCompatActivity {

    private RelativeLayout loadingPanel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        Thread welcomeThread = new Thread() {

            @Override
            public void run() {
                try {
                    super.run();
                    findViewById(R.id.loadingPanel).setVisibility(VISIBLE);
                    sleep(3000);  //Delay of 3 seconds
                } catch (Exception e) {

                } finally {
                    //AccessController view;
                    Intent i = new Intent(LoadingActivity.this,
                            WelcomeActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        };
        setContentView(R.layout.activity_welcome);
        welcomeThread.start();
    }
}
