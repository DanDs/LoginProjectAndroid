package com.blacknight.loginproject;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private String usr;
    private String pswd;
    private TextView e_Message;
    public static final int SIZE_BUCLE_CONEXION = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        e_Message = (TextView) findViewById(R.id.errorMessage);
        e_Message.setText("");
    }

    public void ValidarIngreso(View view) {
        TextView usuario = (EditText) findViewById(R.id.user_field);
        TextView password = (EditText) findViewById(R.id.pswd_field);
        usr = usuario.getText().toString();
        pswd = password.getText().toString();
        if ( this.isConexion(view.getContext())) {
            //Toast.makeText(view.getContext(),"Conexion a internet exitosa", Toast.LENGTH_LONG).show();
            if(usr.isEmpty() || pswd.isEmpty()) {
                if (usr.isEmpty()) {
                    usuario.setError("Ingrese su usuario");
                }
                if (pswd.isEmpty()) {
                    password.setError("Ingrese su password");
                }
            }else {
                if (usr.equals("HOLA") && pswd.equals("nose")) {
                    Intent i = new Intent(view.getContext(), LoadingActivity.class);
                    startActivity(i);
                } else {
                    e_Message.setText("Usuario y/o password invalido");
                    usuario.setText("");
                    password.setText("");
                }
            }
        }
        else{
            Toast.makeText(view.getContext(),"No se tiene conexion a internet, necesita un acceso a Internet para Ingresar", Toast.LENGTH_LONG).show();
        }
    }

    public boolean isConexion(Context ctx) {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        for (int i = 0; i < SIZE_BUCLE_CONEXION; i++) {
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }
}
